## Installation
**Using the terminal**

- `cp .env.example .env`

- Set up the database config in the `.env` file

- `cd` the project directory and run:

	- `composer install`

	- `php artisan migrate`

- run `php artisan serve`


## Import Books CSV File

**Note: make sure you have the file saved in the `/storage/files` directory**

**Using the terminal**

- `cd` the project directory and run `php artisan csv:upload FILE_NAME`


## Using Seeded data to test the API
**After the installation**

- run `php artisan migrate:fresh --seed`