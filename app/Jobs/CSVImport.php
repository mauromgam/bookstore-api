<?php

namespace App\Jobs;

use App\Services\BookService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class CSVImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $ids;

    /**
     * CSVUpload constructor.
     * @param $ids
     */
    public function __construct($ids)
    {
        $this->ids = $ids;
    }

    /**
     * @param BookService $service
     */
    public function handle(BookService $service)
    {
        $values = DB::table('tmp_upload')
            ->whereIn('id', $this->ids)
            ->whereNull('status')
            ->get();

        foreach ($values as $value) {
            $response = $service->upload(json_decode($value->attr));
            if (empty($response)) {
                DB::table('tmp_upload')->where('id', $value->id)->delete();
            } else {
                DB::table('tmp_upload')->where('id', $value->id)->update([
                    'status' => 'failed',
                    'error_message' => json_encode($response)
                ]);
            }
        }
    }
}
