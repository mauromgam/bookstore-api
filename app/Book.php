<?php

namespace App;

use App\Collections\BookCollection;
use App\Collections\PurchaseCollection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $title
 * @property string $info
 * @property PurchaseCollection $purchases
 *
 * Class Book
 * @package App
 */
class Book extends Model
{
    public $table = 'books';

    public $fillable = [
        'title',
        'info',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string',
        'info' => 'required|string',
    ];

    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array $models
     *
     * @return BookCollection
     */
    public function newCollection(array $models = [])
    {
        return new BookCollection($models);
    }

    /**
     * @return array
     */
    public function toDetailsArray()
    {
        return (new BookCollection([$this]))
            ->toDetailsArray()[0];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }
}
