<?php

namespace App\Console\Commands;

use App\Traits\CsvHelpersTrait;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CSVImport extends Command
{
    use CsvHelpersTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'csv:upload {fileName : The name of the file located in the directory "/storage/files"}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload a CSV file of Books';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $relativePathName = storage_path('files');
        $fileName = $this->argument('fileName');

        $this->line('<info>Initializing Import</info>');

        $this->line("<info>File:</info> $relativePathName/$fileName");

        $header = [
            'Title',
            'Info'
        ];

        try {
            $csv = $this->parseCSVFile($fileName, $relativePathName, $header, 25000);
            $records = $csv->getRecords();
            $i = 1;
            $date = date('Y-m-d H:i:s');
            $lastId = DB::select("
                SELECT 
                    `AUTO_INCREMENT` 
                FROM 
                    INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_NAME = 'tmp_upload'
            ");
            $lastId = $lastId[0]->AUTO_INCREMENT;
            foreach ($records as $offset => $record) {
                $status = null;
                $errorMessage = null;
                if (empty($record['Title']) ||
                    empty($record['Info'])
                ) {
                    $status = 'failed';
                }
                if (empty($record['Title'])) {
                    $errorMessage = "Book missing Title.";
                } elseif (empty($record['Info'])) {
                    $errorMessage = "Book missing Info.";
                }
                $attr[] = [
                    'attr'            => json_encode($record),
                    'created_at'      => $date,
                    'status'          => $status,
                    'error_message'   => $errorMessage,
                ];
                if ($i > 2000) {
                    $this->line('<info>Inserting 2000...</info>');
                    DB::table('tmp_upload')->insert($attr);
                    unset($attr);
                    $i = 1;
                }
                $i++;
            }
            if (isset($attr)) {
                $this->line('<info>Inserting ' . count($attr) . '...</info>');
                DB::table('tmp_upload')->insert($attr);
            }
        } catch (\Exception $e) {
            \Log::error($e);
            $this->line('<error>' . $e->getMessage() . '</error>');
            return;
        }
        $value = floor($csv->count()/500);
        if ($value > 0) {
            $remainder = $csv->count()%($value*500);
            for ($i = 1; $i < ($value+1); $i++) {
                $ids = range($lastId, $lastId+500);
                \App\Jobs\CSVImport::dispatch($ids)->onQueue('csvUpload');
                $lastId = $lastId+501;
            }
            if ($remainder > 0) {
                $ids = range($lastId, $lastId+$remainder);
                \App\Jobs\CSVImport::dispatch($ids)->onQueue('csvUpload');
            }
        } else {
            $ids = range($lastId, $lastId+$csv->count());
            \App\Jobs\CSVImport::dispatch($ids)->onQueue('csvUpload');
        }

        $this->line('<info>The End.</info>');
    }
}
