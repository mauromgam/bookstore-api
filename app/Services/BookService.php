<?php

namespace App\Services;

use App\Book;
use App\Repositories\BookRepository;

/**
 * Class BookService
 * @package App\Services
 */
class BookService
{
    /**
     * @var BookRepository
     */
    protected $bookRepository;

    /**
     * BookService constructor.
     * @param BookRepository $bookRepository
     */
    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }


    /**
     * Saves a CSV file content in the Database prior to process it
     *
     * @param $csv
     * @return array
     */
    public function upload($csv)
    {
        $log = [];

        $bookArray = [
            'title' => $csv->Title,
            'info' => $csv->Info
        ];

        try {
            /** Book|null|true $book */
            if ($book = Book::where('title', $csv->Title)->first()) {
                \Log::error("Book already exists - " . $book->id);
                $log['errors'][] = 'Book already exists';
            } else {
                $book = Book::create($bookArray);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error("data: ", $bookArray);
            $log['errors'][] = $e->getMessage();
        }

        return $log;
    }
}
