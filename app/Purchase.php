<?php

namespace App;

use App\Collections\PurchaseCollection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $book_id
 * @property string $email
 * @property Book $book
 *
 * Class Purchase
 * @package App
 */
class Purchase extends Model
{
    public $table = 'purchases';

    public $fillable = [
        'email',
        'book_id',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required|email',
        'book_id' => 'required|int',
    ];

    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array $models
     *
     * @return PurchaseCollection
     */
    public function newCollection(array $models = [])
    {
        return new PurchaseCollection($models);
    }

    /**
     * @return array
     */
    public function toDetailsArray()
    {
        return (new PurchaseCollection([$this]))
            ->toDetailsArray()[0];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
