<?php

namespace App\Repositories;


use App\Book;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class BookRepository extends BaseRepository
{
    /**
     * BaseRepository constructor.
     * @param $model
     */
    public function __construct(Book $model)
    {
        parent::__construct($model);
    }

    /**
     * @param Request $request
     * @param array $columns
     * @param array $sort
     * @return LengthAwarePaginator
     */
    public function getAllPaginated(Request $request, $columns = ['*'], $sort = []): LengthAwarePaginator
    {
        if (empty($sort)) {
            $sort = [
                'title' => 'ASC',
            ];
        }

        return parent::getAllPaginated($request, $columns, $sort);
    }

    /**
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function getRecentlyViewed(Request $request) :?LengthAwarePaginator
    {
        $ids = $request->input('ids');
        if (empty($ids) || !is_array($ids)) {
            return null;
        }

        $this->model = $this->model->whereIn('id', $ids);

        return $this->getAllPaginated($request);
    }
}
