<?php

namespace App\Repositories;


use App\Purchase;
use App\Utils\StringUtil;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PurchaseRepository extends BaseRepository
{
    /**
     * BaseRepository constructor.
     * @param $model
     */
    public function __construct(Purchase $model)
    {
        parent::__construct($model);
    }

    public function getPurchasedBooksByEmail(Request $request)
    {
        $page = isset($request->page) ? $request->page : 1;
        $email = StringUtil::sanitize($request->input('email'));
        if (!$email) {
            return null;
        }

        /** @var Builder $model */
        $model = $this->model->join('books', 'books.id', '=', 'purchases.book_id');
        $model = $model->where('email', '=', $email)
            ->orderBy('books.title')
            ->select([
                'books.*',
                'purchases.email',
                'purchases.created_at as purchase_date',
            ]);

        $results = $model
            ->paginate($request->get('page_size') ?? self::ITEMS_PER_PAGE, ['*'], 'page', $page);
        $this->transformBookListCollection($results);

        return $results;
    }

    /**
     * @param LengthAwarePaginator $paginatedResults
     */
    private function transformBookListCollection(LengthAwarePaginator &$paginatedResults): void
    {
        $collection = $paginatedResults->getCollection();
        $arrayCollection = Collection::make($collection->toBookListArray());
        $paginatedResults->setCollection($arrayCollection);
    }
}
