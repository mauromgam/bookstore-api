<?php

namespace App\Collections;

use App\Book;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BookCollection
 * @package App\Collections
 * @method Book first(callable $callback = null, $default = null)
 */
class BookCollection extends Collection implements CollectionInterface
{
    /**
     * @return array
     */
    public function toBasicArray(): array
    {
        return $this->map(function (Book $book) {
            return [
                'id' => $book->id,
                'title' => $book->title,
                'info' => $book->info,
            ];
        })->all();
    }

    /**
     * @return array
     */
    public function toDetailsArray(): array
    {
        return $this->map(function (Book $book) {
            return [
                'id' => $book->id,
                'title' => $book->title,
                'info' => $book->info,
            ];
        })->all();
    }
}
