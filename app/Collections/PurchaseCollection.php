<?php

namespace App\Collections;

use App\Purchase;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class PurchaseCollection
 * @package App\Collections
 * @method Purchase first(callable $callback = null, $default = null)
 */
class PurchaseCollection extends Collection implements CollectionInterface
{
    /**
     * @return array
     */
    public function toBasicArray(): array
    {
        return $this->map(function (Purchase $purchase) {
            return [
                'id'         => $purchase->id,
                'book_title' => $purchase->book->title,
                'email'      => $purchase->email,
            ];
        })->all();
    }

    /**
     * @return array
     */
    public function toBookListArray(): array
    {
        return $this->map(function (Purchase $purchase) {
            return [
                'id'    => $purchase->id,
                'title' => $purchase->title,
                'info'  => $purchase->info,
                'email' => $purchase->email,
                'date'  => $purchase->purchase_date,
                'timestamp'  => strtotime($purchase->purchase_date),
            ];
        })->all();
    }

    /**
     * @return array
     */
    public function toDetailsArray(): array
    {
        return $this->map(function (Purchase $purchase) {
            return [
                'id'    => $purchase->id,
                'book'  => $purchase->book,
                'email' => $purchase->email,
            ];
        })->all();
    }
}
