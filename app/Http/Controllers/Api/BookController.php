<?php

namespace App\Http\Controllers\Api;

use App\Book;
use App\Http\Controllers\Controller;
use App\Repositories\BookRepository;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /** @var BookRepository $bookRepository */
    protected $bookRepository;

    /**
     * BookController constructor.
     * @param BookRepository $bookRepository
     */
    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $books = $this->bookRepository->getAllPaginated($request);

        return $this->sendResponse($books, 'Books returned successfully.');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recentlyViewed(Request $request)
    {
        $books = $this->bookRepository->getRecentlyViewed($request);

        return $this->sendResponse($books, 'Recently viewed books returned successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate(Book::$rules);

        $book = Book::create($request->all());

        if (!$book) {
            return $this->sendError('Book not created.');
        }

        return $this->sendResponse($book, 'Book created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var Book $book */
        $book = $this->bookRepository->findWithoutFail($id);

        if (!$book) {
            return $this->sendError('Book not found.', 404);
        }

        return $this->sendResponse($book->toDetailsArray(), 'Book returned successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate(Book::$rules);

        /** @var Book $book */
        $book = $this->bookRepository->findWithoutFail($id);

        if (!$book) {
            return $this->sendError('Book not found.', 404);
        }

        $book->update($request->all());

        return $this->sendResponse($book, 'Book updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        /** @var Book $book */
        $book = $this->bookRepository->findWithoutFail($id);

        if (!$book) {
            return $this->sendError('Book not found.', 404);
        }

        $book->delete();

        return $this->sendResponse($book, 'Book updated successfully.');
    }
}
