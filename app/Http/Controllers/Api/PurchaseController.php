<?php

namespace App\Http\Controllers\Api;

use App\Purchase;
use App\Repositories\PurchaseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PurchaseController extends Controller
{
    /** @var PurchaseRepository $purchaseRepository */
    protected $purchaseRepository;

    /**
     * PurchaseController constructor.
     * @param PurchaseRepository $purchaseRepository
     */
    public function __construct(PurchaseRepository $purchaseRepository)
    {
        $this->purchaseRepository = $purchaseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $purchases = $this->purchaseRepository->getAllPaginated($request);

        return $this->sendResponse($purchases, 'Purchases returned successfully.');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPurchasedBooksByEmail(Request $request)
    {
        $purchases = $this->purchaseRepository->getPurchasedBooksByEmail($request);

        return $this->sendResponse($purchases, 'Purchases returned successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate(Purchase::$rules);

        $purchase = Purchase::create($request->all());

        if (!$purchase) {
            return $this->sendError('Purchase not created.', 400, $purchase);
        }

        return $this->sendResponse($purchase, 'Purchase created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var Purchase $purchase */
        $purchase = $this->purchaseRepository->findWithoutFail($id);

        if (!$purchase) {
            return $this->sendError('Purchase not found.', 404);
        }

        return $this->sendResponse($purchase->toDetailsArray(), 'Purchase returned successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate(Purchase::$rules);

        /** @var Purchase $purchase */
        $purchase = $this->purchaseRepository->findWithoutFail($id);

        if (!$purchase) {
            return $this->sendError('Purchase not found.', 404);
        }

        $purchase->update($request->all());

        return $this->sendResponse($purchase, 'Purchase updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        /** @var Purchase $purchase */
        $purchase = $this->purchaseRepository->findWithoutFail($id);

        if (!$purchase) {
            return $this->sendError('Purchase not found.', 404);
        }

        $purchase->delete();

        return $this->sendResponse($purchase, 'Purchase updated successfully.');
    }
}
