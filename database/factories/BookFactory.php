<?php

use Faker\Generator as Faker;

/**
 * Book factory.
 * Used in seeding the clients table.
 */

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'title' => 'Book ' . $faker->randomNumber(7),
        'info'  => $faker->realText(500),
    ];
});
