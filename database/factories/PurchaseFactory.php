<?php

use Faker\Generator as Faker;

/**
 * Book factory.
 * Used in seeding the clients table.
 */

$factory->define(App\Purchase::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'book_id'  => \App\Book::inRandomOrder()->first()->id,
    ];
});
