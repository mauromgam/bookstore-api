<?php

/**
 * All requests (GET/POST/PUT/DELETE) to the books endpoint
 */
Route::resource('books', 'Api\BookController');
// Get recently viewed books
Route::get('recently-viewed', 'Api\BookController@recentlyViewed');

/**
 * All requests (GET/POST/PUT/DELETE) to the books endpoint
 */
Route::resource('purchases', 'Api\PurchaseController');
Route::get('recent-purchases', 'Api\PurchaseController@getPurchasedBooksByEmail');
